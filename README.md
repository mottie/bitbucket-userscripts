# Bitbucket userscripts

Userscripts to add functionality to Bitbucket.

## Installation

1. Make sure you have user scripts enabled in your browser (these instructions refer to the latest versions of the browser):

	* Firefox - install [Greasemonkey](https://addons.mozilla.org/en-US/firefox/addon/greasemonkey/).
	* Chrome - install [Tampermonkey](https://tampermonkey.net/?ext=dhdg&browser=chrome) or [NinjaKit](https://chrome.google.com/webstore/detail/gpbepnljaakggeobkclonlkhbdgccfek).
	* Opera - install [Tampermonkey](https://tampermonkey.net/?ext=dhdg&browser=opera) or [Violent Monkey](https://addons.opera.com/en/extensions/details/violent-monkey/).
	* Safari - install [Tampermonkey](https://tampermonkey.net/?ext=dhdg&browser=safari) or [NinjaKit](http://ss-o.net/safari/extension/NinjaKit.safariextz).
	* Dolphin - install [Tampermonkey](https://tampermonkey.net/?ext=dhdg&browser=dolphin).
	* UC Browser - install [Tampermonkey](https://tampermonkey.net/?ext=dhdg&browser=ucweb).


2. Get information or install:
	* Learn more about the userscript by clicking on the named link. You will be taken to the specific wiki page.
	* Install a script directly from Bitbucket by clicking on the "install" link in the table below.
	* Install a script from [GreasyFork](https://greasyfork.org/en/users/24847-mottie) (GF) from the userscript site page
	* Or, install the scripts from [OpenUserJS](https://openuserjs.org/users/Mottie/scripts) (OU).


	| Userscript Wiki                           | ![][ico] | Direct Install     | Sites                     | Created    | Updated    |
	|-------------------------------------------|:--------:|:------------------:|:-------------------------:|:----------:|:----------:|
	| [Bitbucket collapse in comment][cic-wiki] |          | [install][cic-raw] | [GF][cic-gf] [OU][cic-ou] | 2017.04.28 | 2017.05.16 |
	| [Bitbucket collapse markdown][cmd-wiki]   |          | [install][cmd-raw] | [GF][cmd-gf] [OU][cmd-ou] | 2017.04.28 | 2017.05.16 |

	\* The ![][ico] column indicates that the userscript has been included in the [Octopatcher](https://github.com/Mottie/Octopatcher) browser extension/addon (coming soon).

[cic-wiki]: https://bitbucket.org/mottie/bitbucket-userscripts/wiki/bitbucket_collapse_in_comment
[cmd-wiki]: https://bitbucket.org/mottie/bitbucket-userscripts/wiki/bitbucket_collapse_markdown

[cic-raw]: https://bitbucket.org/mottie/bitbucket-userscripts/raw/HEAD/bitbucket-collapse-in-comment.user.js
[cmd-raw]: https://bitbucket.org/mottie/bitbucket-userscripts/raw/HEAD/bitbucket-collapse-markdown.user.js

[cic-gf]: https://greasyfork.org/en/scripts/29323-bitbucket-collapse-in-comment
[cmd-gf]: https://greasyfork.org/en/scripts/29322-bitbucket-collapse-markdown

[cic-ou]: https://openuserjs.org/scripts/Mottie/Bitbucket_Collapse_In_Comment
[cmd-ou]: https://openuserjs.org/scripts/Mottie/Bitbucket_Collapse_Markdown

[ico]: https://raw.githubusercontent.com/Mottie/Octopatcher/master/src/images/icon16.png

## Updating

Userscripts are set up to automatically update. You can check for updates from within the Greasemonkey or Tampermonkey menu, or click on the install link again to get the update.

Each individual userscript's change log is contained on its individual wiki page.

## Issues

Please report any userscript issues within this repository's [issue section](https://bitbucket.org/mottie/bitbucket-userscripts/issues). Greasyfork messages are also received, but not as easily tracked. Thanks!
